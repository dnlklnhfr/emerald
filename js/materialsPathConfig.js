/*  Configurable material directories [optional]
    --------------------------------------------
    In case you are using different creo-environment-settings where each environment
    uses a different material library (configured in Creo-option 'pro_material_dir')
    but all materials from all libraries should be handled as valid in the tool:
    - Add your topmost material library paths into the Array 'materialDirectories'
      separated by commas
    - Note: Use either single slashes (/) or double backslashes (\\) for the paths

    Example, adding Creo's default material-library:
    'C:/Program Files/PTC/Creo 6.0.2.0/Common Files/text/materials-library/'

    If you leave the Array empty, the tool only checks your 'pro_material_dir'
    for valid material-files
*/
materialDirectories = [
    // 'D:/some/path/to/your/materials',
    // 'K:\\another\\path\\to\\some\\other\\materials_folder'
]