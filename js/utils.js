function getCookie(cName) {  //code copied, but simplified from http://www.w3schools.com/js/js_cookies.asp
    var name = cName + "=";
    var ca = document.cookie.split(';');
    for(var i=ca.length; i--;) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function setCookie(cName, value, expires){
    expires = (expires) ? ' expires='+expires+';' : '';
    document.cookie = cName+"="+value+";" +expires;
}

document.addEventListener('DOMContentLoaded', isStartedInCreo);
function isStartedInCreo(){
    if(window.external.ptc === undefined){
        createAlert('<b>ERROR</b>:\n\
                        No active session detected!\n\
                        Some features will not work as expected.\n\
                        Please start the app in the internal browser of Creo&trade;!',
                    'danger',
                    10000
        )
        return false;
    }

    return true;
}

function setLoadscreen(message){
    let blurTarget = document.querySelector('.contentBody');
    let loadScreen = document.getElementById('loadScreen');

    if(!loadScreen){
        loadScreen = document.createElement('div');
        loadScreen.id = 'loadScreen';
        loadScreen.className = 'loadScreen';
        
        var container = document.createElement('div');
            container.className = 'loadMessageContainer';
        var loadIcon = document.createElement('img');
            loadIcon.src = './img/dnk.svg';
            loadIcon.className = 'loadIcon';
        var loadText = document.createElement('div');
            loadText.className = 'loadText';
            loadText.textContent =  (message) ? message : '';
        blurTarget.classList.add('blurBackground');
        
        container.appendChild(loadIcon);
        container.appendChild(loadText);
        loadScreen.appendChild(container);
        document.body.appendChild(loadScreen);
    }else{
        blurTarget.classList.remove('blurBackground');
        document.body.removeChild(loadScreen);
    }

}

//shitty but works for now .... done: 0090 future should probably consider changing the multilanguage stuff to a global message object to avoid eval()s
function createAsyncAlert(msg, type, duration){
    createAlert(messages[msg][lang], type, duration);
}

//### IE11 polyfills
// Event()-constructor
// https://developer.mozilla.org/en-US/docs/Web/API/Event/Event
// https://developer.mozilla.org/en-US/docs/Web/API/Document/createEvent
// https://developer.mozilla.org/en-US/docs/Web/API/Event/initEvent
(function () {
    if ( typeof window.Event === "function" ) return false; //If not IE

    function Event ( type, options ) {
        options = options || { bubbles: false, cancelable: false, composed: false };
        var evt = document.createEvent( 'Event' );
            evt.initEvent( type, options.bubbles, options.cancelable, options.composed );
        return evt;
    }

    window.Event = Event;
})();

//### Chrome 64 + IE "polyfill"
// surprisingly older versions of Chrome, used in the Creo-backend, don't properly support <datalist>-tags
// therefore added a js-based auto-complete feature for <input>'s for both Chrome + IE to replace <datalist>'s
function autoComplete(input, items) {
    let container = document.createElement('div');
        container.className = 'autoCompleteContainer';
        container.addEventListener('click', selectItem);
    input.parentNode.appendChild(container);
    var activeItemIndex = -1;
    
    input.addEventListener('input', renderItemList);
    input.addEventListener('click', renderItemList);
    input.addEventListener('focus', renderItemList);
    input.addEventListener('keydown', setItem);            

    function renderItemList(event) {
        if(!event.target.readOnly && !event.target.disabled){
            event.stopPropagation();
            // add document-listener only once
            document.removeEventListener('click', removeItemLists);
            document.addEventListener('click', removeItemLists);
            removeItemLists();
            
            container.style.display = 'block';
            container.style.width = getComputedStyle(input).width;
            activeItemIndex = -1;
            let values = getMatches(event.target.value);
            let containerCache = "";
            for(let i=0, count=values.length; i<count; i++){
                containerCache += '<div>'+values[i]+'</div>';  
            }
            container.innerHTML = containerCache;
        }
    }

    function getMatches(value) {
        return items.filter(
            function(item){
                return item.toLowerCase().indexOf(value.toLowerCase().trim()) > -1
            }
        );
    }

    function selectItem(event) {
        if (event.target.tagName === 'DIV') {
            input.removeEventListener('focus', renderItemList);
            input.removeEventListener('input', renderItemList);

            input.value = event.target.textContent;
            input.select();
            removeItemLists();
        }
    }

    function removeItemLists(){
        let itemLists = document.querySelectorAll('.autoCompleteContainer');
        for (let i=0, count=itemLists.length; i<count; i++){
            itemLists[i].innerHTML = '';
            itemLists[i].style.display = 'none';
        }

        window.setTimeout(function() {
            input.removeEventListener('focus', renderItemList);
            input.removeEventListener('input', renderItemList);
            input.addEventListener('focus', renderItemList);
            input.addEventListener('input', renderItemList);
        }, 500);

    }

    function setItem(event){
        let items = container.getElementsByTagName("div");
        switch(event.key){
            case 'ArrowDown':   
            case 'Down':        activeItemIndex++; highlightItem(); break;
            case 'ArrowUp':     
            case 'Up':          activeItemIndex--; highlightItem(); break;
            case 'Esc':
            case 'Escape':      removeItemLists(); break;
            case 'Enter':       event.preventDefault();
                                if(items[activeItemIndex])
                                    items[activeItemIndex].click();
                                removeItemLists();
                                break;
            default: break;
        }

        function highlightItem(){
            activeItemIndex = (activeItemIndex < 0) ? 0
                            : (activeItemIndex >= items.length) ? items.length - 1
                            : activeItemIndex
            for(let i=items.length; i--;)
                items[i].classList.remove('active')

            if(items[activeItemIndex]){
                items[activeItemIndex].classList.add('active');
                container.scrollTop = items[activeItemIndex].offsetTop;
            }
        }
    }

    return container;
}

//### Ordered Object utility to allow ordered keyed data
function OrderedObject() {
    this.data = {};
    this.keys = []; // Array to maintain order
}

OrderedObject.prototype.set = function (key, value, title) {
    if (!this.data.hasOwnProperty(key)) this.keys.push(key);
    this.data[key] = { value: value, title: title };
};

OrderedObject.prototype.forEach = function (callback) {
    for (var i = 0; i < this.keys.length; i++) {
        var key = this.keys[i];
        callback(key, this.data[key].value, this.data[key].title);
    }
};