materialMapping = {
    "al2014": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "al6061": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "brass": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "bronze": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "cu": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "epoxy": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "fe20": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "fe30": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "fe40": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "fe60": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "femall": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "fenodr": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "mg": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "nylon": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "pvc": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "ss": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "steel": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "tially": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "tipure": {
        "type": "Miscellaneous",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "tungsten": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Legacy-Materials/",
        "status": "active"
    },
    "alumina": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "aluminum_nitride": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "boron_carbide": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "borosilicate_glass": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "concrete": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "glass": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "glass_ceramic": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "granite": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "laminated_glass": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "limestone": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "marble": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "sandstone": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "silica_glass": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "silicon_carbide": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "silicon_nitride": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "slate": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "tungsten_carbide": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "zirconia": {
        "type": "Ceramics_and_glasses",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ceramics_and_glasses/",
        "status": "active"
    },
    "cyanate_ester-carbon_composite": {
        "type": "Composites",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Composites/",
        "status": "active"
    },
    "ep-cf_composite": {
        "type": "Composites",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Composites/",
        "status": "active"
    },
    "ep-gf_composite": {
        "type": "Composites",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Composites/",
        "status": "active"
    },
    "pf-gf_composite": {
        "type": "Composites",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Composites/",
        "status": "active"
    },
    "up_composite": {
        "type": "Composites",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Composites/",
        "status": "active"
    },
    "silicone_rubber": {
        "type": "Elastomers_and_rubbers",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Elastomers_and_rubbers/",
        "status": "active"
    },
    "cast_iron_ductile": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "cast_iron_gray": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "cast_iron_malleable": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "cast_iron_nodular": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "stainless_steel_austenitic": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "stainless_steel_ferritic": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "stainless_steel_martensitic": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "steel_hsla": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "steel_cast": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "steel_galvanized": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "steel_high_carbon": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "steel_low_alloy": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "steel_low_carbon": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "steel_low_carbon_heat_treated": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "steel_medium_carbon": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "tool_steel_air_hardening": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "tool_steel_high_speed": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "tool_steel_water_hardening": {
        "type": "Ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Ferrous_metals/",
        "status": "active"
    },
    "pe_foam_flexible": {
        "type": "Foams",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Foams/",
        "status": "active"
    },
    "pf_foam_rigid": {
        "type": "Foams",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Foams/",
        "status": "active"
    },
    "pp_foam_flexible": {
        "type": "Foams",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Foams/",
        "status": "active"
    },
    "ps_foam_rigid": {
        "type": "Foams",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Foams/",
        "status": "active"
    },
    "pu_foam_elastomeric": {
        "type": "Foams",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Foams/",
        "status": "active"
    },
    "pu_foam_rigid": {
        "type": "Foams",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Foams/",
        "status": "active"
    },
    "al-cu_alloy_wrought_hs": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "al-mg-si_alloy_wrought": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "al-si-cu_alloy_cast": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "aluminum_wrought": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "brass_cast": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "brass_wrought": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "bronze_cast": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "copper_cast": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "copper_wrought": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "cu-al_alloy": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "gold": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "lead": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "mg-al-zn_alloy_wrought": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "mg-al_alloy_cast": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "ni-co-cr_alloy_cast": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "ni-co-cr_alloy_wrought": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "ni-cr_alloy": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "ni-cu_alloy": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "nickel": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "pb-sb_alloy_cast": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "silver": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "ti-al-v_alloy": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "tin": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "titanium": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "w-rh_alloy": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "zinc": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "zn-al_alloy_cast": {
        "type": "Non-ferrous_metals",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Non-ferrous_metals/",
        "status": "active"
    },
    "abs": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "ep": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "eva": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "lcp": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pa6-gf": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pa6": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pa66-gf": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pa66": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pbt-gf": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pbt": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pc-abs": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pc": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pe-hd": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pe-ld": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pe-lld": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pe-uhmw": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "peek": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pet": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pf": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pla": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pmma": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pom": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pp": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "ppo-ps_noryl": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pps": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "ps-hi": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "ps": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "ptfe_teflon": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pvc-p_flexible": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "pvc-u_rigid": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "san": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "tps": {
        "type": "Plastics",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Plastics/",
        "status": "active"
    },
    "bamboo": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "cork": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "hardboard": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "hardwood-birch": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "hardwood-cherry": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "hardwood-maple": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "hardwood-oak": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "hardwood-walnut": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "plywood": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    },
    "softwood-pine": {
        "type": "Woods",
        "path": "$PRO_DIRECTORY/text/materials-library/Standard-Materials_Granta-Design/Woods/",
        "status": "active"
    }
}
