/***** messagefile for tools DE/EN *****/

//### create CSS styles for language settings on-the-fly
//### this applies only to css-classes 'de' && 'en' for German and English language
var css = document.createElement('style');
css.type = 'text/css';

var styles = ' .en, .de { display:none; }';                   // step1: hide all elements with a language class
styles += ' .en:lang(en), .de:lang(de) { display:inline-block; }';   // step2: show the elements that match with the lang-attribute set in the html-tag

if (css.styleSheet) css.styleSheet.cssText = styles;
else css.appendChild(document.createTextNode(styles));
document.getElementsByTagName("head")[0].appendChild(css);

//### retrieve user language and set html 'lang'-attribute accordingly
try{
    if(localStorage instanceof Storage)
        var lang = (JSON.parse(localStorage.getItem('options'))['language'].indexOf("de") > -1) ? 'de' : 'en';
    else
        var lang = (JSON.parse(getCookie('options'))['language'].indexOf("de") > -1) ? 'de' : 'en';
}catch(e){
    //simplified language detection via browser settings as a fallback
    var lang = (window.navigator.language.indexOf("de") > -1)?'de':'en';
    console.log(e)
}
if(lang == 'de'){
    document.getElementsByTagName("html")[0].lang = "de";
}else{
    document.getElementsByTagName("html")[0].lang = "en";
}

//### create a language selection menu on-the-fly or hide it, if already visible
//### all ids/css-classes are located/referenced in basicLayout.css
function selectLang(htmlElement){
    if(document.getElementById("selLangMenu") == null){
        var selLangMenu = document.createElement("span");
        var langDE = document.createElement("span");
        var langEN = document.createElement("span");
        var divider = document.createElement("span");

        selLangMenu.id = "selLangMenu";

        langDE.appendChild(document.createTextNode("DE"));
        langDE.onclick = function(){
            emerald.controller.options['language'] = "de";
            emerald.controller.appSettings.storage.setItem('options', JSON.stringify(emerald.controller.options));
            location.reload();
        }
        langDE.className = "languageItem";

        divider.appendChild(document.createTextNode("|"));

        langEN.appendChild(document.createTextNode("EN"));
        langEN.onclick = function(){
            emerald.controller.options['language'] = "en";
            emerald.controller.appSettings.storage.setItem('options', JSON.stringify(emerald.controller.options));
            location.reload();
        }
        langEN.className = "languageItem";

        selLangMenu.appendChild(langDE);
        selLangMenu.appendChild(divider);
        selLangMenu.appendChild(langEN);


        htmlElement.appendChild(selLangMenu);

        selLangMenu.style.left = htmlElement.offsetLeft - selLangMenu.offsetWidth - 5 +"px";
        selLangMenu.style.top =  htmlElement.offsetTop + htmlElement.offsetHeight/2 - selLangMenu.offsetHeight/2 +"px";
    }else{
        var element = document.getElementById("selLangMenu");
            element.parentNode.removeChild(element);
    }
}

//### Message section - every message is listed in German and English.
//### depending on the user selection and the variable 'lang', the correct text will be used.
//### lang == true: German; lang == false: English

messages = {
    glob_active_model_not_solid: {
        de: 'Aktuelles Modell ist kein Part oder eine Baugruppe!',
        en: 'Active model is not a part or an assembly!'
    },
    glob_no_model_loaded: {
        de: 'Kein Modell geladen!',
        en: 'No model loaded!'
    },
    glob_name: {
        de: 'Benennung',
        en: 'Name'
    },
    glob_component: {
        de: 'Komponente',
        en: 'Component'
    },
    glob_copied_to_clipboard: {
        de: 'In die Zwischenablage kopiert: ',
        en: 'Copied to clipboard: '
    },
    glob_export_success:{
        de: 'Export erfolgreich',
        en: 'Export successful'
    },
    glob_cancel: {
        de: 'Abbruch',
        en: 'Cancel'
    },
    glob_confirm: {
        de: 'OK',
        en: 'OK'
    },
    glob_edit: {
        de: 'Bearbeiten',
        en: 'Edit'
    },
    glob_try_again: {
        de: 'Versuchs nochmal :)',
        en: 'Please try again'
    },
    glob_multiselect: {
        de: 'STRG/Umschalt+LMT für Mehrfachselektion',
        en: 'CTRL/Shift+LMB for multiselect'
    },
    glob_nothing_selected: {
        de: 'Keine Auswahl getroffen!',
        en: 'Nothing selected!'
    },
    glob_multi_edit: {
        de: 'Mehrfach editieren',
        en: 'Multi-Editing'
    },
    gm_component_actions: {
        de: 'Modell-<br/>aktionen',
        en: 'Model actions'
    },
    gm_mass: {
        de: 'Masse',
        en: 'Mass'
    },
      gm_volume: {
        de: 'Volumen',
        en: 'Volume'
    },
      gm_surface_area: {
        de: 'Oberflaeche',
        en: 'Surface\narea'
    },
      gm_material: {
        de: 'Creo Material',
        en: 'Creo material'
    },
    gm_select_autorepair_options: {
        de : 'Waehle Autoreparatur-Optionen',
        en : 'Select autorepair options to apply'
    },
    gm_dlg_autorepair_conditions: {
        de : 'Autoreparatur-Regeln',
        en : 'Autorepair conditions'
    },
    gm_dlg_autorepair_densities: {
        de : 'Standards fuer fehlende Werkstoffe (<span class="materialStateWarning">N/A</span>) nach Dichtebereich:',
        en : 'Select defaults for missing materials (<span class="materialStateWarning">N/A</span>) by density range:'
    },
    gm_dlg_autorepair_invalid: {
        de : 'Alternativen fuer erkannte ungueltige Werkstoffe:',
        en : 'Select alternatives for detected invalid materials:'
    },
    gm_simprep_detected: {
        de: 'WARNUNG: Vereinfachte Darstellung aktiv!\nModelle werden nur in Master-Darstellung korrekt ausgewertet',
        en: 'WARNING: Simplified Rep detected!\nEnsure Master-Rep on models for proper evaluation'
    },
    gm_apply_material_success: {
        de: 'Material erfolgreich dem Modell zugewiesen.\nFuer aktualisierte Masseneigenschaften bitte Auswertung erneut ausfuehren <span onclick=\"emerald.controller.getCadModelInfo()\" class=\"material-icons-outlined mi-play-circle-outline iconStyle editable\"></span>',
        en: 'Material assigned successfully to model.\nPlease re-run <span onclick=\"emerald.controller.getCadModelInfo()\" class=\"material-icons-outlined mi-play-circle-outline iconStyle editable\"></span> mass-analysis for updatet mass props!'
    },
    gm_model_not_in_session: {
        de: 'Materialzuweisung fehlgeschlagen\nModell nicht in Sitzung gefunden\n',
        en: 'Material assignment failed!\nModel not found in session\n'
    },
    gm_density_limit_exceeded: {
        de: 'FEHLER:\nunplausibler Dichtewert',
        en: 'ERROR:\nunreasonable density'
    },
    gm_mat_missing: {
        de: 'WARNUNG:\nKein Werkstoff zugewiesen',
        en: 'WARNING:\nNo material assigned'
    },
    gm_save_to_file: {
        de: 'Ergebnisse als Datei speichern',
        en: 'Save results to file'
    },
    gm_load_from_file: {
        de: 'Aus Datei laden',
        en: 'Load from File'
    },
    gm_load_from_file_to_display: {
        de: 'JSON-Datei zum Anzeigen laden',
        en: 'Load JSON File to display'
    },
    gm_select_json_file: {
        de: 'JSON-Datei waehlen',
        en: 'Select JSON-file'
    },
    gm_load_from_file_title: {
        de: 'Bestehende Ergebnisse aus Datei laden',
        en: 'Load existing results from a file'
    },
    gm_languageSelect_title:{
        de: 'Sprache waehlen',
        en: 'Select language'
    },
    gm_settings:{
        de: 'Einstellungen',
        en: 'Settings'
    },
    gm_general:{
        de: 'Allgemein',
        en: 'General'
    },
    gm_parts:{
        de: 'Einzelteile',
        en: 'Parts'
    },
    gm_assemblies:{
        de: 'Baugruppen',
        en: 'Assemblies'
    },
    gm_save:{
        de: 'Speichern',
        en: 'Save'
    },
    gm_cancel:{
        de: 'Abbrechen',
        en: 'Cancel'
    },
    gm_apply:{
        de: 'Anwenden',
        en: 'Apply'
    },
    gm_load:{
        de: 'Laden',
        en: 'Load'
    },
    gm_export:{
        de: 'Exportieren',
        en: 'Export'
    },
    gm_close:{
        de: 'Schliessen',
        en: 'Close'
    },
    gm_settings_title:{
        de: 'Tool-Einstellungen',
        en: 'Tool settings'
    },
    gm_about_title:{
        de: 'Infos zum Tool',
        en: 'Infos about this tool'
    },
    gm_compare_with_file: {
        de: 'Aktives Ergebnis mit Datei vergleichen',
        en: 'Compare current results with data from file'
    },
    gm_export_to_excel: {
        de: 'Ergebnisse in Excel-kompatible Datei exportieren',
        en: 'Export results as Excel-compatible file'
    },
    gm_apply_filter: {
        de: 'Filter anwenden',
        en: 'Apply configurable filters to report'
    },
    gm_find_icon: {
        de: 'Textsuche...\n\n'+
            'Suchoptionen:\n'+
            '- Wert    .. Texteingabe\n'+
            '- Abfragen mit #\n'+
            '    .. #Status [ok, warning, error]\n'+
            '    .. #Merkmal:Wert/Status\n'+
            '    .. #Merkmal:!Wert/Status (invers)\n\n'+
            'Beispiele im Dropdown-Menue',
        en: 'Find elements in report ...\n\n'+
            'Search-options:\n'+
            '- By value    .. Enter text\n'+
            '- By query with #\n'+
            '        .. #status [ok, warning, error]\n'+
            '        .. #property:value/status\n'+
            '        .. #property:!value/status (inverted)\n\n'+
            'Examples in the drop-down-menu'
    },
    gm_find_term: {
        de: 'Suchbegriff eingeben/selektieren',
        en: 'Enter/Select search term ...'
    },
    gm_find_prop: {
        de: 'Merkmal: ',
        en: 'Property: '
    },
    gm_find_prop_invalid: {
        de: '\nexistiert nicht.\n\nInfo auf den Spaltenueberschriften',
        en: '\ndoesn\'t exist.\n\nPlease check hints on column-headers.'
    },
      gm_start_analysis: {
        de: 'Analyse starten',
        en: 'Start Analysis'
    },
    gm_start_analysis_title: {
        de: 'Massenauswertung starten',
        en: 'Start the mass analysis'
    },
    gm_calculating_results: {
        de: 'Auslesen der Modelldaten ...',
        en: 'Reading model data ...'
    },
    gm_suggest_material_change_in_generic: {
        de: 'Aenderungen für Familientabellenvarianten sollten im generischen Teil erfolgen!\nAndernfalls koennen Fehler in der Auswertung auftreten.',
        en: 'Changes on family table instances should be performed in the generic part!\nOtherwise errors may occur in the analysis.'
    },
    gm_hint_level: {
        de: 'Strukturebene\n\nSuchbegriff:\n#',
        en: 'Structural level\n\nSearch key:\n#'
    },
    gm_hint_component: {
        de: 'Dateiname\n\nSuchbegriff:\n#',
        en: 'Filename\n\nSearch key:\n#'
    },
    gm_hint_component_actions: {
        de: 'Komponentenaktionen:\nOeffnen, Hervorheben, Modellinfo',
        en: 'Component actions:\nOpen, Highlight, Model info'
    },
    gm_hint_open_model: {
        de: 'Modell oeffnen',
        en: 'Open model'
    },
    gm_hint_highlight_model: {
        de: 'Modell hervorheben',
        en: 'Highlight model'
    },
    gm_hint_model_info: {
        de: 'Modellinfo anzeigen',
        en: 'Show model info'
    },
    gm_hint_name: {
        de: 'Ueblicher Name\n\nSuchbegriff:\n#',
        en: 'Common name\n\nSearch key:\n#'
    },
    gm_hint_mass: {
        de: 'Masse/Gewicht der Komponente\n\nSuchbegriff:\n#',
        en: 'Mass/weight of component\n\nSearch key:\n#'
    },
    gm_hint_volume: {
        de: 'Volumen der Geomtrie\n\nSuchbegriff:\n#',
        en: 'Volume of geometry\n\nSearch key:\n#'
    },
    gm_hint_surface: {
        de: 'Oberflaeche der Geomtrie\n\nSuchbegriff:\n#',
        en: 'Surface area of geometry\n\nSearch key:\n#'
    },
    gm_hint_material: {
        de: 'Aktuell zugewiesener Werkstoff\n\nSuchbegriff:\n#',
        en: 'Currently assigned material\n\nSearch key:\n#'
    },
    gm_hint_material_group: {
        de: 'Typzugehoerigkeit des Werkstoffs\n\nSuchbegriff:\n#',
        en: 'Type classification of material\n\nSearch key:\n#'
    },
    gm_hint_density: {
        de: 'Modelldichte\n\nSuchbegriff:\n#',
        en: 'Model density\n\nSearch key:\n#'
    },
    gm_hint_characteristic: {
        de: 'Werkstoffeigenschaft\n\nSuchbegriff:\n#',
        en: 'Material characteristic\n\nSearch key:\n#'
    },
    gm_hint_inertia_moment: {
        de: 'Haupttraegheitsmoment um angegebene Achse\n\nSuchbegriff:\n#',
        en: 'Moment of inertia around given axis\n\nSearch key:\n#'
    },
    gm_hint_inertia_product: {
        de: 'Deviationsmoment fuer angegebene Achsen\n\nSuchbegriff:\n#',
        en: 'Product of inertia for given axes\n\nSearch key:\n#'
    },
    gm_hint_cog: {
        de: 'Massenschwerpunkt mit Rerefenz zu [siehe Tool-Optionen]\n\nSuchbegriff:\n#',
        en: 'Center of Gravity referring to [see tool-options]\n\nSearch key:\n#'
    },
    gm_material_group: {
        de: 'Materialgruppe',
        en: 'Material group'
    },
    gm_excel_material_groups: {
        de: 'Materialgruppen',
        en: 'Material groups'
    },
    gm_density: {
        de: 'Dichte',
        en: 'Density'
    },
    gm_poisson_ratio: {
        de: 'Querkontrak-<br/>tionszahl',
        en: 'Poisson<br/>ratio'
    },
    gm_young_modulus: {
        de: 'E-Modul',
        en: 'Young\nModulus'
    },
    gm_thermal_exp_coeff: {
        de: 'Waermeausdehnungs-<br/>koeffizient',
        en: 'Therm.Exp.<br/>Coeff.'
    },
    gm_ixx: {
        de: 'Ixx',
        en: 'Ixx'
    },
    gm_iyy: {
        de: 'Iyy',
        en: 'Iyy'
    },
    gm_izz: {
        de: 'Izz',
        en: 'Izz'
    },
    gm_ixy: {
        de: 'Ixy',
        en: 'Ixy'
    },
    gm_ixz: {
        de: 'Ixz',
        en: 'Ixz'
    },
    gm_iyz: {
        de: 'Iyz',
        en: 'Iyz'
    },
    gm_center_of_gravity_x: {
        de: 'Schw.p. X',
        en: 'CoG X'
    },
    gm_center_of_gravity_y: {
        de: 'Schw.p. Y',
        en: 'CoG Y'
    },
    gm_center_of_gravity_z: {
        de: 'Schw.p. Z',
        en: 'CoG Z'
    },
    gm_legend_label: {
        de: 'Legende',
        en: 'Legend'
    },
    gm_legend_text: {
        de: 'Legende: \n'+
                'blau:      Baugruppen \n' +
                'gelb:      Baugruppen oberster Ebene \n' +
                'rot:        Dichtewerte > 0.00001 \n' +
                'orange:  Fehlende Creo-Materialien \n' +
                'grün:      relevantes Massentraegheitsmoment bei ungefaehr \n' +
                '              rotationssymmetrischen Teilen (BETA)',
        en: 'Legend:  \n'+
                'blue:      assemblies \n' +
                'yellow:   top level assemblies \n' +
                'red:        density > 0.00001 \n' +
                'orange:  missing Creo-materials \n' +
                'green:    relevant moment of inertia of nearly \n' +
                '              rotationally symmetric models (BETA)'
    },
    gm_export_select_format: {
        de: 'Ausgabeformat waehlen:',
        en: 'Select output format:'
    },
      gm_export_all: {
        de: 'Alles',
        en: 'All'
    },
      gm_export_visible: {
        de: 'Nur Sichtbares',
        en: 'Visible only'
    },
      gm_export_select_content: {
        de: 'Inhalt waehlen:',
        en: 'Select content:'
    },
      gm_material_group: {
        de: 'Materialgruppe',
        en: 'Material group'
    },
      gm_results_no_guarantee: {
        de: 'Angaben ohne Gewaehr.',
        en: 'Information provided without guarantee.'
    },
    gm_verify_with_pivot: {
        de: 'Die Summen koennen mit Pivot-Tabellen verifiziert werden',
        en: 'You can verify these numbers with Pivot-tables'
    },
      gm_excel_material_groups: {
        de: 'Materialgruppen',
        en: 'material groups'
    },
    gm_material_multiassign: {
        de: 'Werkstoff zur mehrfachen Zuweisung zu Komponenten waehlen',
        en: 'Choose material for multiassignment to components'
    },
    gm_insert_material: {
        de: 'Werkstoff eingeben...',
        en: 'Insert material...'
    },
    gm_copy_to_clip_acc_3: {
        de: 'Klick:\nKopiert Wert mit 3 Nachkommastellen in die Zwischenablage',
        en: 'Click:\nCopy value to clipboard with 3 digits'
    },
      gm_copy_to_clip_acc_2: {
        de: 'Klick:\nKopiert Wert mit 2 Nachkommastellen in die Zwischenablage',
        en: 'Click:\nCopy value to clipboard with 2 digits'
    },
    gm_density_modified: {
        de: 'WARNUNG:\nDichte wurde manuell veraendert oder ist veraltet!',
        en: 'WARNING:\nDensity has been modified manually or is outdated!'
    },
      gm_density_file: {
        de: 'Dichte Mat.Datei\t',
        en: 'Density Mat.File:\t'
    },
    gm_density_model: {
        de: 'Dichte Modell\t',
        en: 'Density Model:\t'
    },
    gm_invalid_materials_found: {
        de: 'Nicht mehr existente Creo-Materialien gefunden!\nBitte ersetze die <span style=\"background-color:#FFBBBB\">rot markierten</span> Werkstoffe und versuchs nochmal.\n',
        en: 'Nonexistent Creo-materials found!\nPlease replace the <span style=\"background-color:#FFBBBB\">red marked</span> materials and try again.\n'
    },
    gm_mass_analysis_success: {
        de: 'Massenauswertung erfolgreich abgeschlossen!',
        en: 'Mass analysis completed successfully!'
    },
    gm_material_changed: {
        de: 'Werkstoff geaendert',
        en: 'Material changed'
    },
    gm_from: {
        de: 'von:\t',
        en: 'from:\t'
    },
      gm_to: {
        de: 'zu:\t',
        en: 'to:\t'
    },
    gm_density_updated: {
        de: 'Modelldichte aktualisiert\nWerkstoff unveraendert',
        en: 'Model density updated.\nMaterial unchanged.'
    },
    gm_drawing_model_used: {
        de: 'Aktives Zeichnungsmodell wird verwendet',
        en: 'Used active drawing model instead'
    },
    gm_material_invalid: {
        de: 'FEHLER:\nWerkstoff nicht\nin Creo-Bibliothek',
        en: 'ERROR:\nMaterial not\nin Creo-Library'
    },
    gm_material_not_in_library: {
        de: 'FEHLER:\nWerkstoff nicht in Creo Bibliothek\n\nCreo Config-Option pro_material_dir pruefen!',
        en: 'ERROR:\nMaterial not in Creo library\n\nCheck Creo config-option pro_material_dir'
    },
    gm_material_groups_eval_wrong: {
        de: 'Auswertung der Werkstoffgruppen ist u.U. fehlerhaft!',
        en: 'Clustered results may be wrong!'
    },
    gm_opt_label_quickAccess: {
        de: 'Schnellzugriff aktivieren',
        en: 'Enable QuickAccess'
    },
    gm_opt_descr_quickAccess: {
        de: 'Aktions-Buttons zum Oeffnen, Hervorheben und Untersuchen des jeweiligen Modells hinzufuegen',
        en: 'Add action-buttons to each component to open, highlight and investigate the model'
    },
    gm_opt_label_digits: {
        de: 'Anzahl Nachkommastellen',
        en: 'Number of digits'
    },
    gm_opt_descr_digits: {
        de: 'Steuern der Nachkommastellen von numerischen Werten im Ergebnis (Ausnahme: Dichte)',
        en: 'Control the number of digits diplayed for numerical values in the results (except density)'
    },
    gm_opt_label_evalMaterialGroups: {
        de: 'Materialkategorien auswerten',
        en: 'Evaluate material groups'
    },
    gm_opt_descr_evalMaterialGroups: {
        de: 'Abgleich mit Materialtyp-Definition fuer jeden Werkstoff zur Gruppierung nach Typ \
            (z.B. Stahl, Aluminium, Kunststoff, ...). Voraussetzung: Eine gepflegte Mappingliste von Werkstoff->Werkstofftyp',
        en: 'Match material type definitions to each material to aggregation by type (i.e. Steel, Aluminum, Plastics, ...). \
            This requires a maintained list of material->materialType matches.'
    },
    gm_opt_label_evalFamilyTables: {
        de: 'Familientabellen-Varianten auswerten',
        en: 'Evaluate family table instances'
    },
    gm_opt_descr_evalFamilyTables: {
        de: 'Nur fuer Einzelteile - Auswerten der Masseneigenschaften fuer alle Varianten eines generischen Teils (enthaelt eine Familientabelle) ',
        en: 'Single parts only - evaluate mass data for all instances of a generic part (contains a family table)'
    },
    gm_opt_label_hideSkeletons: {
        de: 'Skelettmodelle ausblenden',
        en: 'Hide skeleton models'
    },
    gm_opt_descr_hideSkeletons: {
        de: 'Modells des Typs \'Skelettmodell\' ausschliessen, da diese per definition keine Masse haben',
        en: 'Exclude models of type \'Skeleton Model\' as they don\'t have a mass by definition'
    },
    gm_opt_label_cogToTopLevel: {
        de: 'Massenschwerpunkt relativ zu Hauptbaugruppe',
        en: 'Center of Gravity towards top-level'
    },
    gm_opt_descr_cogToTopLevel: {
        de: 'Berechnen des Massenschwerpunkts bezogen auf das Koordinatensystem der obersten (Haupt-)Baugruppe \
            Standardmaessig wertet Creo zum Koordinaten-Ursprung der jeweiligen Komponente aus.',
        en: 'Calculate the Center of Gravity relatively to the coordinate-system of the top-most assembly. \
            Creo-default is relative to component-coordinate system of each component.'
    }


}