# What is Emerald?
**E**xtended **m**aterial **e**diting, **r**epair and **a**nalysis for **l**ightweight **d**esigns

_Emerald_ is a javascript-based extension for the CAD design software <nobr>PTC CREO&reg; </nobr> using the native API Creo.JS.<br>
It helps with applying proper material-definitions to the parts of an entire CAD-assembly.
<br><br>

**A typical scenario:**

You want to analyze the mass properties of a large assembly containing hundreds or even thousands of parts.
Usually there are many models of various origins.<br>
Then you'd probably want to hit the button-sequence &nbsp; <nobr>```Analysis > Mass Properties```</nobr> &nbsp; on the top-most assembly to get its nominal weight 
and then most likely you'll face the same results you'll always get once you're trying this: <br> 
Your assembly has a weight of several tons above the expected value.<br> 
Now the tedious chapter of this suspectedly simple task starts: finding the culprit causing the immense weight deviation among all those parts.


This is the moment _Emerald_ comes in handy:
- List mass properties for each component of an assembly with just one click
- Find missing or invalid material assignments
- Apply quick repair actions
- Edit material assignments with just a click
- Get insights about material groups
- Save or export your analysis

Learn more about how to use these features [below](#features)


# Content

[TOC]


# Getting started

Using _Emerald_ is fairly simple - no installation required.<br> 
You can either download the content of this repository as \*.zip:

[emerald-master.zip](https://gitlab.com/dnlklnhfr/emerald/-/archive/master/emerald-master.zip)

or clone it via console-command to your preferred destination with:

```bash
git clone https://gitlab.com/dnlklnhfr/emerald.git
```

Technically you can place the downloaded files almost everywhere, i.e. on a local harddrive, sharedrive, webserver or cloud-space.<br>
Unless you want to use your own material-libaries and material type definitions, you're ready to start using _Emerald_.<br>
For custom configurations, you may want to have a look into the chapter [Configuration](#configuration)

To start _Emerald_ simply navigate to `emerald.html` in the embedded browser in Creo&reg;.
<br><br>

> :warning: **Warning:**
>
> Running an application using Creo.JS the first time, you might face a 'Creo.JS Security Warning'.<br>
> In order to run any such application, including _Emerald_, this warning needs to be confirmed with `yes`. <br> 
> _Emerald_ is designed with the best intentions to help you. <br> 
> However you are solely responsible for the decision whether you want to use this interface or not.<br>
> In any case it is highly recommended to save your work before using any kind of script.


# Configuration

Organizations typically use their own corporate configurations for Creo, contained in \*.pro-files.<br>
Sometimes even multiple configurations can exist for different environments.<br> 
This can lead to scenarios like:
- different configurations with different material-libraries (folders containing material-files [\*.mtl, \*.mat]) but all materials should be valid
- new materials are being added to the libraries
- outdated materials might be removed

Selectable valid materials in _Emerald_ are maintained in a configurable material-mapping-list which itself depends on material-library-paths.<br>

## 1. Configure material-library-paths

1. Navigate to `js/materialsPathConfig.js`
2. Add required library-paths to the `materialDirectories`-Array
3. Save the file

If you leave the `materialDirectories`-Array empty, _Emerald_ will try to find new materials for the [mapping](#2-map-materials-to-material-types-groups) in the path set in the Creo-option `pro_material_dir`.

## 2. Map materials to material types (groups)

By default materials located at `<creo-loadpoint>\Common Files\text\materials-library` are considered as valid materials.<br> 
In case you want to change this or you want to modify the predefined material types, follow these steps first:
1. Open `js/materialMapping.js` in your preferred code-editor
2. Replace the entire content with 
    ```javascript
    materialMapping={}
    ```
3. Save the file


For frequently updated material-libraries the following steps should be performed regularly.<br>
If you're material-library is very stable, this has to be done only once for your environment.

1. Start _Emerald_ in admin-mode by accessing it with `emerald.html?admin`
2. Click the [Update Materials]-Button
3. Map new materials to their respective material types (groups) as described in the dialog window
4. Save the file as `materialMapping.js` to the `js`-folder of your _Emerald_-copy. <br>
You can overwrite the file - your new mappings have been appended to the existing ones.

It is recommended to keep your _Emerald_-files in a place with version control in order to keep track of your changes.


# Features

## New analysis from session

Simply click on [Start Analysis] - the bigger your assembly the more time it might take.<br>
More features will be available after execution.

![New Analysis Animation](/docs/new_analysis.gif "New analysis")

## Edit material assignments

Please note: <br>
It might be necessary to regenerate your assembly after changing material assignments in order to see the changes in a new [new analysis](#new-analysis-from-session)-call with _Emerald_.
This heavily depends on your environment configuration regarding auto-regeneration after changes of mass properties.

### Single-edit

You can modify the material assignment for each part indiviually.<br>
As part names can only exist once per session, all instances of one part will be updated automatically.

![Single-edit](/docs/single_edit.gif "Single-edit")

### Multi-edit

Materials can be assigned to multiple parts at once.<br>
Range selections also work conventiently on filtered or collapsed views.

![Multi-edit](/docs/multi_edit.gif "Multi-edit")

## Search and filter

The search feature includes a full-text search for the results as well a lightweight query-language.<br> 
This way you are able to find components not only by free text-search but also by properties like their material status.<br> 
Using the query-syntax to search in a particular column this acts as a column-filter.<br> 
You'll find the respective column-identification-keys by hovering the column headers.

Query-search works with a preceding hashtag (#) and the syntax is 
- `#key:value` for positive matches
- `#key:!value` for inverted matches (note the exclamation mark)

`value` can be simply a text-content or a status like one of `[ok, warning, error]`.

Some frequently required queries can be directly selected in the drop-down-menu.<br> 
Example: Find components where the material status is _not_ 'ok' <br> 
As a query this is expressed by:  `#material:!ok`

![Search](/docs/search.gif "Search")

## Model actions

Still not sure with which models you're dealing with? Model actions reveal details for each component.<br>
For example: Highlight Model

![Highlight Model](/docs/model_actions.gif "Highlight Model")

## Auto repair

Define default materials for some boundary conditions.<br> 
Simply leave fields empty for rules you don't want to be applied.

![Auto Repair](/docs/auto_repair.gif "Auto Repair")

## Save and Load an analysis

You can save your analysis as JSON-file. These saved analyses preserve their tree-fold-state.<br> 
Saved analyses can be loaded for review or customized exports much faster than a new calculation from session.

## Export analysis

Export your analyses into tabular formats like CSV or Xml-Spreadsheet.<br> 
These formats can be typically opened or imported to your favorite table-editor.<br>
Choose what content you want to export. Visibility can be changed by tree-folding or filters.

![Export analysis](/docs/export.png "Export Analysis")


# Compatibility

_Emerald_ should work with Creo Parametric&trade; 4.0 M100 and higher.<br> 
Both browser types `[ie_browser, chromium_browser]` for Creo-option `windows_browser_type` are basically supported.<br> 
As both browser types used inside Creo are implemented in fairly old versions (InteretExplorer11 and Chrome v64) you might experience some unexpected behaviour.

In case you are facing any problems, please sign up with [GitLab](https://gitlab.com/users/sign_up/) and submit a new [Issue](https://gitlab.com/dnlklnhfr/emerald/-/issues/new) to this repository.


# Troubleshooting

- **_Emerald_ simply doesn't work**<br> 
    You may have declined the 'Creo.JS Security Warning' mentioned in [Getting Started](#getting-started) somewhere in the past.<br>
    Search for a file named `accessible_urls.json` in your `%appdata%`-folder and delete that json-file. The warning should then show up again.
- **Random javascript-errors from Creo.JS**<br> 
    Using InternetExplorer as the `windows_browser_type` can collide with the URL's listed in the 'Compatiblity View Settings' of IE.<br> 
    Make sure to exclude the directory or domain where you have hosted _Emerald_ from that list in IE (Located within `IE > Settings > Compatiblity View settings`).



# Known Issues (so far ...)

It's hard to guess what kind of configurations you might have in your environment and what kind of sophisticated techniques you might have used in your CAD-designs.
Probably there are many more scenarios than the ones tested while developing _Emerald_.<br> 
However, here are a few constellations that do not always behave as expected:

- **Mixed unit systems**<br>
    This first version of _Emerald_ is designed to support the SI-unit-system [mmks]. Other systems or even assemblies with mixed systems or materials are not tested.
- **Weight deviations with 'flexible components'**<br> 
    The feature 'Make Flexible' in Creo for parts like i.e. compression springs might cause deviations between the sum of all single part weights and the calculated weight of the assembly. This happens as the 'flexiblity' actually changes the calculated volume of the flexibly assembled component in the context of the parent assembly compared to the same component opened in a separate window.
- **Assembly family tables**<br> 
    Family tables on parts work quite well but due some yet unknown circumstances it might happen that family tables applied on assemblies can cause unexpected occurrences in the mass report of _Emerald_. So better have an eye on it if you use this feature.
- **Cabling**<br> 
    Wires and hoses created with the Creo module 'Cabling' won't show any weight in _Emerald_. Cabling-parts do not have a classic material assignment and therefore
    no explicit density applied. The weight of Cabling-parts depends on so called 'Spools' that define a 'weight per length'-approach.
    Unfortunately I haven't figured out so far how to get the calculated weight of a Cabling-part with Creo.JS without too much extra efforts.


There might be more cases where the results of _Emerald_ are not matching the expectations. I'd be happy for feedback if you face some strange behaviour.


# Dependencies

_Emerald_ uses some amount of external code which is also freely available under the terms of the respective licenses noted next to each project listed below.
The external code sections are being shipped with this repository complying with the applicable licenses.

| Resource    | Modifications | Source | License |
| -----------               | ----------- | -----------| --------|
| **Google Material Icons** | Used subset:<br> 'Outlined font icons' | https://github.com/google/material-design-icons <br> Maintained in:<br> https://github.com/marella/material-icons| [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
| **Lala Alerts**           | See comments in source | https://github.com/lalaman/lala-alerts-js | [MIT License](https://opensource.org/licenses/MIT)
| **Creo.JS** | None | Installation directory of Creo Parametric | Licence type unknown - distribution granted in <br>Creo.JS User's Guide, chapter '[Initialize Creo.JS Framework](/creojs/CreoJS_distribution_note.png)'<br> located in:<br> `<creo-loadpoint>/creojs/creojsug.pdf` |

# Some final thoughts

This is my first bigger project and more important (to me) my first official open source project.<br> 
My goal with _Emerald_ is an easy-to-use but powerful tool to simplify the tedious work of analysing mass property details of large CAD-assemblies designed in Creo&trade;.
Although provided on this mainly software-oriented platform GitLab, _Emerald_ is actually for all the mechanical engineers out there (so am I by the way).<br>
So another goal was to get _Emerald_ working without various external dependencies, software deployment-tools or "complex" installation processes.<br> 
Hope that worked out :)

# License

_Emerald_ can be used completely free of charge under the terms of the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0).<br>
You are welcome to share, contribute or just leave a feedback.


# Legal Footnote

PTC, Creo and Creo Parametric are trademarks or registered trademarks of PTC Inc. or its subsidiaries in the U.S. and in other countries.

Emerald is _NOT_ affiliated with PTC Inc. - Please _DO NOT_ contact PTC Inc. for support.

# Changelog

| Version | Changes |
| --- | --- |
| 0.1.1 | <li>Various bugfixes</li> |
| 0.1.0 | <li>First publish</li> |
